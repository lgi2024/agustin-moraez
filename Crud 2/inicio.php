<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CRUD</title>
    <style>
        body {
            background-color: lightblue;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        table, th, td {
            border: 1px solid black;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
        /* Estilos para resaltar las filas */
        .highlight {
            background-color: lightyellow;
        }
    </style>
    <script>
        function highlightRow(row) {
            row.classList.add('highlight');
        }

        function unhighlightRow(row) {
            row.classList.remove('highlight');
        }

        function confirmDelete() {
            return confirm("¿Estás seguro de que deseas eliminar este registro?");
        }
    </script>
</head>
<body>
<?php
// Incluir la creación de la base de datos y la tabla estudiantes
require 'creacion.php';

// Iniciar sesión y verificar si el usuario está logueado
session_start();

if (!isset($_SESSION['user_id'])) {
    die("No estás logueado.");
}

$is_admin = isset($_SESSION['is_admin']) ? $_SESSION['is_admin'] : 0;

// Query a la base de datos
$query = "SELECT id, nombre, edad, email, foto FROM estudiantes";
$result = mysqli_query($conn, $query);

if ($result) {
    echo "<p>Lista de estudiantes: </p>";
    
    if ($is_admin) {
        echo "<p>
            <form style='display:inline;' action='agregar.php' method='get'> 
                <button type='submit'>Agregar</button>
            </form>
            <form style='display:inline;' action='buscar.php' method='get'> 
                <button type='submit'>Buscar</button>
            </form>
        </p>";
    }
    
    echo "<table>";
    echo "<tr><th>Foto</th><th>Nombre</th><th>Edad</th><th>Email</th><th>DECISIÓN</th></tr>";
    
    // Mostrar los resultados
    while ($row = mysqli_fetch_assoc($result)) {
        echo "<tr onmouseover='highlightRow(this)' onmouseout='unhighlightRow(this)' onmousedown='highlightRow(this)' onmouseup='unhighlightRow(this)'>";
        // Mostrar la imagen si existe
        echo "<td><img src='uploads/" . htmlspecialchars($row['foto']) . "' width='50' height='50'></td>";
        echo "<td>" . htmlspecialchars($row['nombre']) . "</td>";
        echo "<td>" . htmlspecialchars($row['edad']) . "</td>";
        echo "<td>" . htmlspecialchars($row['email']) . "</td>";
        echo "<td>";

        if ($is_admin) {
            echo "<form style='display:inline;' action='editar.php' method='get'>
                    <input type='hidden' name='id' value='" . $row['id'] . "'>
                    <button type='submit'>Editar</button>
                  </form>
                  <form style='display:inline;' action='borrar.php' method='POST' onsubmit='return confirmDelete();'>
                    <input type='hidden' name='id' value='" . $row['id'] . "'>
                    <button type='submit'>Eliminar</button>
                  </form>";
        }
        
        echo "</td>";
        echo "</tr>";
    }
    echo "</table>";
} else {
    echo "Error retrieving data: " . mysqli_error($conn);
}

// Cierre de la base de datos
mysqli_close($conn);
?>
</body>
</html>
