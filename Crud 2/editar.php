<?php
// Incluir archivo de conexión a la base de datos
require 'creacion.php';

$id = $nombre = $edad = $email = $foto = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = mysqli_real_escape_string($conn, $_POST['id']);
    $nombre = mysqli_real_escape_string($conn, $_POST['nombre']);
    $edad = mysqli_real_escape_string($conn, $_POST['edad']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);

    // Procesar la imagen
    if (isset($_FILES['foto']) && $_FILES['foto']['error'] == 0) {
        $foto = $_FILES['foto']['name'];
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($foto);

        if (move_uploaded_file($_FILES['foto']['tmp_name'], $target_file)) {
            $foto = mysqli_real_escape_string($conn, $foto);
            // Actualizar la base de datos con la nueva imagen
            $query = "UPDATE estudiantes SET nombre='$nombre', edad='$edad', email='$email', foto='$foto' WHERE id='$id'";
        } else {
            echo "Error al subir la imagen.";
        }
    } else {
        // Si no se sube una nueva imagen, actualizar solo los otros campos
        $query = "UPDATE estudiantes SET nombre='$nombre', edad='$edad', email='$email' WHERE id='$id'";
    }

    if (mysqli_query($conn, $query)) {
        header('Location: inicio.php');
        exit;
    } else {
        echo "Error al editar estudiante: " . mysqli_error($conn);
    }
}

if (isset($_GET['id'])) {
    $id = mysqli_real_escape_string($conn, $_GET['id']);
    $query = "SELECT id, nombre, edad, email, foto FROM estudiantes WHERE id='$id'";
    $result = mysqli_query($conn, $query);
    if ($result && mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $nombre = $row['nombre'];
        $edad = $row['edad'];
        $email = $row['email'];
        $foto = $row['foto'];
    } else {
        echo "No se encontró estudiante con ID: " . $id;
    }
}

// Cerrar la conexión a la base de datos
mysqli_close($conn);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Editar Estudiante</title>
    <style>
        body {
            background-color: lightblue;
        }
    </style>
</head>
<body>
    <h2>Editar Estudiante</h2>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre" value="<?php echo htmlspecialchars($nombre); ?>" required><br><br>
        
        <label for="edad">Edad:</label>
        <input type="number" id="edad" name="edad" value="<?php echo htmlspecialchars($edad); ?>" required><br><br>
        
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" value="<?php echo htmlspecialchars($email); ?>" required><br><br>
        
        <label for="foto">Foto de perfil:</label>
        <input type="file" id="foto" name="foto"><br><br>
        <?php if ($foto): ?>
            <img src="uploads/<?php echo htmlspecialchars($foto); ?>" width="50" height="50" alt="Foto de perfil">
        <?php endif; ?>
        
        <button type="submit">Guardar Cambios</button>
    </form>
</body>
</html>
