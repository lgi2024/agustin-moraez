<?php
// Incluir archivo de conexión a la base de datos
require 'creacion.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = mysqli_real_escape_string($conn, $_POST['id']);

    // Obtener el nombre del archivo de la imagen
    $query = "SELECT foto FROM estudiantes WHERE id='$id'";
    $result = mysqli_query($conn, $query);
    if ($result && mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $foto = $row['foto'];
        
        // Eliminar la imagen del servidor
        $file_path = "uploads/" . $foto;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
    }

    // Eliminar el registro de la base de datos
    $query = "DELETE FROM estudiantes WHERE id='$id'";
    if (mysqli_query($conn, $query)) {
        header('Location: inicio.php');
        exit;
    } else {
        echo "Error al borrar estudiante: " . mysqli_error($conn);
    }
}

// Cerrar la conexión a la base de datos
mysqli_close($conn);
?>

