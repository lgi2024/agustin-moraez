<?php
// Incluir archivo de conexión a la base de datos
require 'creacion.php';

$results = [];

// Obtener todos los estudiantes de la base de datos
$query = "SELECT * FROM estudiantes";
$result = mysqli_query($conn, $query);

if ($result) {
    $results = mysqli_fetch_all($result, MYSQLI_ASSOC);
} else {
    echo "Error en la consulta: " . mysqli_error($conn);
}

mysqli_close($conn);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Buscar Estudiante</title>
    <style>
        body {
            background-color: lightblue;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        table, th, td {
            border: 1px solid black;
        }
        th, td {
            padding: 10px;
            text-align: left;
        }
        .highlight {
            background-color: lightyellow;
        }
    </style>
    <script>
        let estudiantes = <?php echo json_encode($results); ?>;

        function buscarEstudiantes() {
            let inputNombre = document.getElementById("busqueda").value.toLowerCase();
            let inputEdad = document.getElementById("edad").value;
            let inputEmail = document.getElementById("email").value.toLowerCase();
            let ordenarPor = document.getElementById("ordenar_por").value;

            let filtrados = estudiantes.filter(estudiante => {
                let nombre = estudiante.nombre.toLowerCase().includes(inputNombre);
                let edad = !inputEdad || estudiante.edad >= inputEdad;
                let email = !inputEmail || estudiante.email.toLowerCase().includes(inputEmail);

                return nombre && edad && email;
            });

            mostrarResultados(filtrados, ordenarPor);
        }

        function mostrarResultados(filtrados, ordenarPor) {
            if (ordenarPor) {
                filtrados.sort((a, b) => (a[ordenarPor] > b[ordenarPor]) ? 1 : -1);
            }

            const tbody = document.getElementById("resultados");
            tbody.innerHTML = "";

            filtrados.forEach(estudiante => {
                let fila = document.createElement("tr");
                fila.onmouseover = () => fila.classList.add('highlight');
                fila.onmouseout = () => fila.classList.remove('highlight');

                fila.innerHTML = `
                    <td><img src="uploads/${estudiante.foto}" width="50" height="50" alt="Foto de perfil"></td>
                    <td>${estudiante.id}</td>
                    <td>${estudiante.nombre}</td>
                    <td>${estudiante.edad}</td>
                    <td>${estudiante.email}</td>
                `;
                tbody.appendChild(fila);
            });
        }
    </script>
</head>
<body>
    <h2>Buscar Estudiante</h2>
    <form onsubmit="event.preventDefault(); buscarEstudiantes();">
        <label for="nombre">Nombre:</label>
        <input type="text" id="busqueda" onkeyup="buscarEstudiantes()"><br><br>
        
        <label for="edad">Edad (>=):</label>
        <input type="number" id="edad" onkeyup="buscarEstudiantes()"><br><br>
        
        <label for="email">Email:</label>
        <input type="email" id="email" onkeyup="buscarEstudiantes()"><br><br>

        <label for="ordenar_por">Ordenar por:</label>
        <select id="ordenar_por" onchange="buscarEstudiantes()">
            <option value="edad">Edad</option>
            <option value="nombre">Nombre</option>
            <option value="email">Email</option>
        </select><br><br>

        <input type="button" value="Buscar" onclick="buscarEstudiantes()">
    </form>

    <h2>Resultados de la Búsqueda</h2>
    <table>
        <thead>
            <tr>
                <th>Foto</th>
                <th>ID</th>
                <th>Nombre</th>
                <th>Edad</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody id="resultados"></tbody>
    </table>
    
    <br>
    <a href="inicio.php">Volver al Inicio</a>
</body>
</html>

