<?php
// Incluir el archivo de conexión
require 'loginDb.php';

// Reabrir la conexión para el login
$conn = mysqli_connect($servername, $username, $password, $dbname);

// Verificar conexión
if (!$conn) {
    die("Conexión fallida: " . mysqli_connect_error());
}

// Función para limpiar entradas de usuario
function sanitize_input($data) {
    global $conn;
    return mysqli_real_escape_string($conn, trim($data));
}

// Manejar el envío del formulario de login y registro
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['login'])) {
        $nombre = sanitize_input($_POST['nombre']);
        $contrasena = sanitize_input($_POST['contrasena']);

        // Consultar la base de datos
        $sql = "SELECT id, contrasena, is_admin FROM datos WHERE nombre='$nombre'";
        $result = mysqli_query($conn, $sql);

        if ($result && $row = mysqli_fetch_assoc($result)) {
            // Verificar la contraseña
            if (password_verify($contrasena, $row['contrasena'])) {
                // Iniciar sesión
                session_start();
                $_SESSION['user_id'] = $row['id'];
                $_SESSION['is_admin'] = $row['is_admin']; // Almacenar si es administrador o no
                header("Location: inicio.php");
                exit;
            } else {
                echo "Credenciales incorrectas.";
            }
        } else {
            echo "Credenciales incorrectas.";
        }
    } elseif (isset($_POST['register'])) {
        $nombre = sanitize_input($_POST['nombre']);
        $contrasena = sanitize_input($_POST['contrasena']);
        $is_admin = isset($_POST['admin']) ? 1 : 0; // 1 para administrador, 0 para usuario normal

        // Verificar si el nombre de usuario ya existe
        $stmt = $conn->prepare("SELECT * FROM datos WHERE nombre=?");
        $stmt->bind_param("s", $nombre);
        $stmt->execute();
        $result_check = $stmt->get_result();
        
        if ($result_check->num_rows > 0) {
            echo "El nombre de usuario ya está en uso.";
        } else {
            // Hash de la contraseña
            $hashed_password = password_hash($contrasena, PASSWORD_DEFAULT);
            
            // Insertar nuevo usuario
            $stmt = $conn->prepare("INSERT INTO datos (nombre, contrasena, is_admin) VALUES (?, ?, ?)");
            $stmt->bind_param("ssi", $nombre, $hashed_password, $is_admin);
            
            if ($stmt->execute()) {
                echo "Registro exitoso!";
            } else {
                echo "Error al registrar el usuario: " . $conn->error;
            }
        }
    }
}

// Cerrar la conexión
mysqli_close($conn);
?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Login y Registro</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            background-color: #f4f4f4;
        }
        .container {
            display: flex;
            border: 1px solid #ccc;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            border-radius: 8px;
            overflow: hidden;
        }
        .form-container {
            flex: 1;
            padding: 20px;
        }
        .form-container h2 {
            margin-top: 0;
        }
        .separator {
            border-left: 1px solid #ddd;
            height: 100%;
        }
        .form-container label {
            display: block;
            margin-bottom: 5px;
        }
        .form-container input[type="text"],
        .form-container input[type="password"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ddd;
            border-radius: 4px;
        }
        .form-container input[type="submit"] {
            background-color: #007bff;
            color: #fff;
            border: none;
            padding: 10px 20px;
            border-radius: 4px;
            cursor: pointer;
        }
        .form-container input[type="submit"]:hover {
            background-color: #0056b3;
        }
        .container h2 {
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <!-- Formulario de Login -->
        <div class="form-container">
            <h2>Iniciar sesión</h2>
            <form method="POST" action="">
                <label for="nombre">Nombre:</label>
                <input type="text" id="nombre" name="nombre" required>
                <br>
                <label for="contrasena">Contraseña:</label>
                <input type="password" id="contrasena" name="contrasena" required>
                <br>
                <input type="submit" name="login" value="Iniciar sesión">
            </form>
        </div>

        <!-- Línea Vertical -->
        <div class="separator"></div>

        <!-- Formulario de Registro -->
        <div class="form-container">
    <h2>Registrarse</h2>
    <form method="POST" action="">
        <label for="nombre_registro">Nombre:</label>
        <input type="text" id="nombre_registro" name="nombre" required>
        <br>
        <label for="contrasena_registro">Contraseña:</label>
        <input type="password" id="contrasena_registro" name="contrasena" required>
        <br>
        <label for="admin">Administrador:</label>
        <input type="checkbox" id="admin" name="admin">
        <br>
        <input type="submit" name="register" value="Registrarse">
    </form>
</div>
    </div>
</body>
</html>
