<?php
$servername = "localhost";
$username = "root";
$password = "admin";
$dbname = "escuela";

// Crear conexión
$conn = mysqli_connect($servername, $username, $password);

// Verificar conexión
if (!$conn) {
    die("Conexión fallida: " . mysqli_connect_error());
}

// Comprobar si la base de datos existe
$db_check_query = "SHOW DATABASES LIKE '$dbname'";
$result = mysqli_query($conn, $db_check_query);

if (mysqli_num_rows($result) == 0) {
    // Crear base de datos si no existe
    $sql = "CREATE DATABASE $dbname";
    if (!mysqli_query($conn, $sql)) {
        die("Error al crear la base de datos: " . mysqli_error($conn));
    }
}

// Seleccionar la base de datos
mysqli_select_db($conn, $dbname);

// Crear tabla estudiantes
$sql = "CREATE TABLE IF NOT EXISTS estudiantes (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    edad INT NOT NULL,
    email VARCHAR(100) NOT NULL,
    foto VARCHAR(255) NULL
)";

if (!mysqli_query($conn, $sql)) {
    die("Error al crear la tabla: " . mysqli_error($conn));
}
?>


