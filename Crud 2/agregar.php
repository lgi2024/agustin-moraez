<?php
// Incluir archivo de conexión a la base de datos
require 'creacion.php';

$nombre = $edad = $email = $foto = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Recibir y limpiar datos del formulario
    $nombre = mysqli_real_escape_string($conn, $_POST['nombre']);
    $edad = mysqli_real_escape_string($conn, $_POST['edad']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);

    // Procesar la imagen
    if (isset($_FILES['foto']) && $_FILES['foto']['error'] == 0) {
        $foto = $_FILES['foto']['name'];
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($foto);

        // Mover el archivo subido a la carpeta 'uploads'
        if (move_uploaded_file($_FILES['foto']['tmp_name'], $target_file)) {
            $foto = mysqli_real_escape_string($conn, $foto);
        } else {
            echo "Error al subir la imagen.";
        }
    }

    // Insertar datos en la base de datos
    $query = "INSERT INTO estudiantes (nombre, edad, email, foto) VALUES ('$nombre', '$edad', '$email', '$foto')";
    if (mysqli_query($conn, $query)) {
        // Redirigir de vuelta a inicio.php después de agregar
        header('Location: inicio.php');
        exit;
    } else {
        echo "Error al agregar estudiante: " . mysqli_error($conn);
    }
}

// Cerrar la conexión a la base de datos
mysqli_close($conn);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Agregar Estudiante</title>
    <style>
        body {
            background-color: lightblue;
        }
    </style>
</head>
<body>
    <h2>Agregar Estudiante</h2>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre" required><br><br>
        
        <label for="edad">Edad:</label>
        <input type="number" id="edad" name="edad" required><br><br>
        
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required><br><br>
        
        <label for="foto">Foto de perfil:</label>
        <input type="file" id="foto" name="foto"><br><br>
        
        <button type="submit">Agregar</button>
    </form>
</body>
</html>



