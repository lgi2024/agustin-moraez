<?php
// Configuración de la base de datos
$servername = "localhost";
$username = "root";
$password = "admin";
$dbname = "login";

// Crear conexión
$conn = mysqli_connect($servername, $username, $password);

// Verificar conexión
if (!$conn) {
    die("Conexión fallida: " . mysqli_connect_error());
}

// Comprobar si la base de datos existe y crearla si es necesario
$db_check_query = "SHOW DATABASES LIKE '$dbname'";
$result = mysqli_query($conn, $db_check_query);

if (mysqli_num_rows($result) == 0) {
    // Crear base de datos si no existe
    $sql = "CREATE DATABASE $dbname";
    if (!mysqli_query($conn, $sql)) {
        die("Error al crear la base de datos: " . mysqli_error($conn));
    }
}

// Seleccionar la base de datos
mysqli_select_db($conn, $dbname);

// Crear tabla si no existe
$sql = "CREATE TABLE IF NOT EXISTS datos (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL UNIQUE,
    contrasena VARCHAR(255) NOT NULL,
    is_admin TINYINT(1) NOT NULL DEFAULT 0
)";

if (!mysqli_query($conn, $sql)) {
    die("Error al crear la tabla: " . mysqli_error($conn));
}

// Insertar un usuario de ejemplo (solo para pruebas)
$sql = "INSERT IGNORE INTO datos (nombre, contrasena) VALUES ('usuario', '" . password_hash('contrasena', PASSWORD_DEFAULT) . "')";
if (!mysqli_query($conn, $sql)) {
    die("Error al insertar el usuario: " . mysqli_error($conn));
}

// Cerrar la conexión
mysqli_close($conn);
?>
